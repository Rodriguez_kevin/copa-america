package negocio;

import java.util.ArrayList;
import java.util.Collections;


public class Fixture {
	private ArrayList<Pais> _paises;
	private Pais[][] _distribucion;
	private Pais[][] _mejorDistribucion;
	private double[] _dispercionGrupo;
	private double _mejorDispercion;
		
	public Fixture(ArrayList<Pais> paises) 
	{
		int filas=3;
		int columnas=4;
		this._paises=paises;
		this.set_distribucion(new Pais[filas][columnas]);
		this.set_mejorDistribucion(new Pais[filas][columnas]);
		this.set_dispercionGrupo(new double[3]);
	}
	
	public void crearFixture() 
	{
		distribuir();
		mejorDispercion();
		fuerzaBruta();
	}
	
	public void distribuir() 
	{
		int indice=0;
		for (int i = 0; i < get_distribucion().length; i++) {
		for (int j = 0; j < get_distribucion()[0].length; j++) {
			get_distribucion()[i][j]=_paises.get(indice);
			indice++;
			}
		}
	}
	
	public void fuerzaBruta() 
	{
		int conbinacionesPorFila=5;
		for (int i = 0; i < conbinacionesPorFila; i++) 
		{
			for (int j = 0; j < conbinacionesPorFila; j++) 
			{
				for (int k = 0; k < conbinacionesPorFila; k++) 
				{
					for (int l = 0; l < conbinacionesPorFila; l++) 
					{
						int colum=3;
						swap(colum,l);
						mejorDispercion(); 
					}
					int colum=2;
					swap(colum,k);
					mejorDispercion(); 
					
				}
				int colum=1;
				swap(colum,j);
				mejorDispercion(); 
			}
			int colum=0;
			swap(colum,i);
			mejorDispercion(); 
		}
	}
	
	public void swap(int colum, int caso) 
	{	
		Pais aux;
 		if(caso == 0) { 
			aux = get_distribucion()[1][colum]; 
			get_distribucion()[1][colum]=get_distribucion()[2][colum];
			get_distribucion()[2][colum]=aux; 
		}
		else if(caso == 1) {  
			aux = get_distribucion()[0][colum]; 
			get_distribucion()[0][colum]=get_distribucion()[1][colum];
			get_distribucion()[1][colum]=aux; 
		}
		else if(caso == 2) { 
			aux = get_distribucion()[1][colum]; 
			get_distribucion()[1][colum]=get_distribucion()[2][colum];
			get_distribucion()[2][colum]=aux; 
		}
		else if(caso == 3) {  
			aux = get_distribucion()[0][colum]; 
			get_distribucion()[0][colum]=get_distribucion()[1][colum];
			get_distribucion()[1][colum]=aux; 
		}
		else if(caso == 4) {  
			aux = get_distribucion()[1][colum]; 
			get_distribucion()[1][colum]=get_distribucion()[2][colum];
			get_distribucion()[2][colum]=aux; 
		}		
	}
	
	private void mejorDispercion() 
	{
		double minimo=0;
		ArrayList<Double> disperciones= new ArrayList<Double>();
		disperciones.add(calcularDispercion(get_distribucion()[0]));
		disperciones.add(calcularDispercion(get_distribucion()[1]));
		disperciones.add(calcularDispercion(get_distribucion()[2]));
		minimo=Collections.min(disperciones);
		
		if(_mejorDispercion==0 && get_mejorDistribucion()== null) 
		{
			_mejorDispercion=minimo;
			copiarArrays(disperciones);
			CopiarMatriz();
		}
		else if(minimo>_mejorDispercion) 
		{
			_mejorDispercion=minimo;
			copiarArrays(disperciones);
			CopiarMatriz();
		}		
	}
	
	public double calcularDispercion(Pais[] grupo) 
	{
		double dispercion=0;
		double promedio= calcularPromedio(grupo);
		for (Pais pais : grupo) 
			dispercion+=(Math.pow((pais.getIniceFueza()-promedio),2))/3; 
		
		return dispercion;
	}
	
	public double calcularPromedio(Pais[] grupo) 
	{
 		double promedio=0;
 		for (Pais pais : grupo) 
 			promedio= promedio + pais.getIniceFueza();
		
 		return promedio/4;
	}

	public void CopiarMatriz() 
	{
		for (int i = 0; i < get_distribucion().length; i++) 
		for (int j = 0; j < get_distribucion()[i].length; j++) 
				get_mejorDistribucion()[i][j]= get_distribucion()[i][j];
	}
	
	public void copiarArrays(ArrayList<Double> lista) 
	{
		int puntero=0;
		for (Double double1 : lista) {
			get_dispercionGrupo()[puntero]=double1;
			puntero++;
		}
	}

	public Pais[][] get_mejorDistribucion() {
		return _mejorDistribucion;
	}

	public void set_mejorDistribucion(Pais[][] _mejorDistribucion) {
		this._mejorDistribucion = _mejorDistribucion;
	}

	public double[] get_dispercionGrupo() {
		return _dispercionGrupo;
	}

	public void set_dispercionGrupo(double[] _dispercionGrupo) {
		this._dispercionGrupo = _dispercionGrupo;
	}

	public Pais[][] get_distribucion() {
		return _distribucion;
	}

	public void set_distribucion(Pais[][] _distribucion) {
		this._distribucion = _distribucion;
	}
}










