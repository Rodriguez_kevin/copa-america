package negocio;

import javax.swing.Icon;

public class Pais {
	private String nombre;
	private int iniceFueza;
	private Icon bandera;
	
	
	public Pais(String nombre,int indice,Icon bandera){
		this.nombre= nombre;
		this.iniceFueza=indice;
		this.bandera= bandera;
	
	}
	public boolean equals(Pais otro) {
		if(this.nombre==otro.getNombre())
			return true;
		else 
			return false;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIniceFueza() {
		return iniceFueza;
	}

	public void setIniceFueza(int iniceFueza) {
		this.iniceFueza = iniceFueza;
	}

	public Icon getBandera() {
		return bandera;
	}

	public void setBandera(Icon bandera) {
		this.bandera = bandera;
	}
	

}
