package test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import interfas.PantallaPrincipal;
import negocio.Fixture;
import negocio.Pais;

public class FixtureTest 
{
	ArrayList<Pais> _paises=arrayPaises();
	
	@Test
	public void PromedioTest() {
		Fixture fixture = new Fixture(_paises);
		double promedio=fixture.calcularPromedio(listaPaises());
		double esperado= (55+63+59+12)/4;
		assertEquals(esperado, promedio,2);
		}
	
	@Test
	public void SwapTest() {
		Fixture fixture = new Fixture(_paises);
		fixture.distribuir();
		fixture.swap(3, 0);  //columna 3; caso 0
		assertTrue(compararSwap(fixture.get_distribucion()));
	}
	
	public Pais[] listaPaises() {
		Pais[] paises= new Pais[4];
		paises[0]= (new Pais("Argentina",55,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/argentina.png"))));
		paises[1]=(new Pais("Brasil",63,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/brasil.png"))));
		paises[2]=(new Pais("Uruguay",59,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/uruguay.png"))));
		paises[3]=(new Pais("Paraguay",12,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/paraguay.png"))));
		return paises;
	}
	private boolean compararSwap(Pais[][] distribucion) {
		if (distribucion[1][3].equals(_paises.get(11)) && distribucion[2][3].equals(_paises.get(7))) {
			return true;
		}
		else 
			return false;
	}
	public ArrayList<Pais> arrayPaises()
	{
		_paises= new ArrayList<Pais>();
		_paises.add(new Pais("Argentina",55,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/argentina.png"))));
		_paises.add(new Pais("Brasil",63,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/brasil.png"))));
		_paises.add(new Pais("Uruguay",59,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/uruguay.png"))));
		_paises.add(new Pais("Paraguay",12,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/paraguay.png"))));
		_paises.add(new Pais("Chile",24,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/chile.png"))));
		_paises.add(new Pais("Bolivia",3,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/bolivia.png"))));
		_paises.add(new Pais("Peru",26,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/peru.png"))));
		_paises.add(new Pais("Colombia",36,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/colombia.png"))));
		_paises.add(new Pais("Venezuela",15,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/venezuela.png"))));
		_paises.add(new Pais("Ecuador",6,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/ecuador.png"))));
		_paises.add(new Pais("Japon",21,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/japon.png"))));
		_paises.add(new Pais("Qatar",11,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/qatar.png"))));
		return _paises;
	}

}
