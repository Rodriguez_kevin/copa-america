package interfas;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import negocio.Fixture;
import negocio.Pais;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;


public class PantallaPrincipal extends JFrame {

	private JPanel contentPane;
	private ArrayList<Pais> _paises=new ArrayList<Pais>();
	private Pais[][] _distribucion= new Pais[3][4];
	private JLabel[][] _banderas= new JLabel[3][4];
	private Fixture fixture;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PantallaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PantallaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PantallaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PantallaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPrincipal frame = new PantallaPrincipal();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PantallaPrincipal() {
		setTitle("                               CREADOR DE GRUPOS DE LA COPA AMERICA");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1083, 681);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.DARK_GRAY);
		setContentPane(contentPane);
		setIconImage(Toolkit.getDefaultToolkit().getImage(PantallaPrincipal.class.getResource("/imagenes/icono.png")));
		contentPane.setLayout(null);
		
		JButton btnGrupo = new JButton("Generar grupos");
		btnGrupo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarPaises();
				generarFixture();
				mostrarBanderas(_distribucion);
			}
		});
		btnGrupo.setForeground(Color.yellow);
		btnGrupo.setBackground(Color.GREEN);
		btnGrupo.setBounds(897, 38, 133, 35);
		contentPane.add(btnGrupo);
		equipos();
		btnTitulos();
		
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/fondoCopa.jpg")));
		lblNewLabel.setBounds(0, 0, 1142, 657);
		contentPane.add(lblNewLabel);
	}
	
	private void agregarPaises() 
	{
		_paises.add(new Pais("Argentina",55,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/argentina.png"))));
		_paises.add(new Pais("Brasil",63,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/brasil.png"))));
		_paises.add(new Pais("Uruguay",59,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/uruguay.png"))));
		_paises.add(new Pais("Paraguay",12,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/paraguay.png"))));
		_paises.add(new Pais("Chile",24,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/chile.png"))));
		_paises.add(new Pais("Bolivia",3,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/bolivia.png"))));
		_paises.add(new Pais("Peru",26,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/peru.png"))));
		_paises.add(new Pais("Colombia",36,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/colombia.png"))));
		_paises.add(new Pais("Venezuela",15,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/venezuela.png"))));
		_paises.add(new Pais("Ecuador",6,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/ecuador.png"))));
		_paises.add(new Pais("Japon",21,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/japon.png"))));
		_paises.add(new Pais("Qatar",11,new ImageIcon(PantallaPrincipal.class.getResource("/imagenes/qatar.png"))));
	}
	
	public void generarFixture()
	{
		fixture= new Fixture(_paises);
		fixture.crearFixture();
		_distribucion= fixture.get_mejorDistribucion();
		
	}
	
	public void btnTitulos() 
	{
		JButton btnGrupoA = new JButton("Grupo A");
		btnGrupoA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int nroGrupo=0;
				if(fixture!=null)
					JOptionPane.showMessageDialog(null,estadisticasGrupo(nroGrupo),"", JOptionPane.INFORMATION_MESSAGE );
			}

		});
		btnGrupoA.setBounds(536, 215, 89, 23);
		contentPane.add(btnGrupoA);
		
		JButton btnGrupoB = new JButton("Grupo B");
		btnGrupoB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int nroGrupo=1;
				if(fixture!=null)
					JOptionPane.showMessageDialog(null,estadisticasGrupo(nroGrupo),"", JOptionPane.INFORMATION_MESSAGE );
			}

		});
		btnGrupoB.setBounds(715, 215, 89, 23);
		contentPane.add(btnGrupoB);
		
		JButton btnGrupoC = new JButton("Grupo C");
		btnGrupoC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int nroGrupo=2;
				if(fixture!=null)
					JOptionPane.showMessageDialog(null,estadisticasGrupo(nroGrupo),"", JOptionPane.INFORMATION_MESSAGE );
			}

		});
		btnGrupoC.setBounds(894, 215, 89, 23);
		contentPane.add(btnGrupoC);	
	}
	
	public void equipos()
	{
		int lat = 530;
		for (int i = 0; i < _banderas.length; i++) {
			int lon = 250;
			for (int j = 0; j < _banderas[i].length; j++) {
				_banderas[i][j] = new JLabel("");
				_banderas[i][j].setBounds(lat, lon, 102, 74);
				contentPane.add(_banderas[i][j]);
				lon += 100;
			}
			lat+=180;
		}
	}
	
	public void mostrarBanderas(Pais[][] fixture) 
	{
		for (int i = 0; i < _banderas.length; i++) {
			for (int j = 0; j < _banderas[i].length; j++) {
				_banderas[i][j].setIcon(fixture[i][j].getBandera());
			}
		}
	}

	private String estadisticasGrupo(int nroGrupo)
	{
		String ret="Grupo A conformado por: \n" 
	+_distribucion[nroGrupo][0].getNombre()+": Indice de Fuerza: "+_distribucion[nroGrupo][0].getIniceFueza()+"\n"
	+_distribucion[nroGrupo][1].getNombre()+": Indice de Fuerza: "+_distribucion[nroGrupo][1].getIniceFueza()+"\n"
	+_distribucion[nroGrupo][2].getNombre()+": Indice de Fuerza: "+_distribucion[nroGrupo][2].getIniceFueza()+"\n"
	+_distribucion[nroGrupo][3].getNombre()+": Indice de Fuerza: "+_distribucion[nroGrupo][3].getIniceFueza()+"\n"
	+"Dispersion del grupo:" + Math.round(fixture.get_dispercionGrupo()[nroGrupo]);
		return ret;
	}
	
}	
 
